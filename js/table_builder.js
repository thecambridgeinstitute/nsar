(function ($){
    function toggleCompareButton(btn) {
        if (btn.find("span").hasClass("glyphicon-plus")) {
            btn.html('<span class="glyphicon glyphicon-minus"></span>');
            window.compareFilter[btn.attr("name")] = 1;
        } else {
            btn.html('<span class="glyphicon glyphicon-plus"></span>');
            delete window.compareFilter[btn.attr("name")];
        }
        $("#compareInput").val("");
        var keys = [];
        for(var k in window.compareFilter) keys.push(k);
        $("#compareInput").val(keys.join(","));
    }
 
    var updateTable = function(dbString, queryString, qid, tableID) {
        var baseURL = "https://thecambridgeinstitute.quickbase.com/db/" + dbString + "?";
        var authString = "?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&";
        var action = "a=API_GenResultsTable&";
        var query = "query=" + queryString + "&";
        var format = "jsa=1&";
        var semester = (queryString === "") ? "qid=" + qid + "&" : "";
        var clist;
        if (tableID === "#PSARTable") {
		   clist = "clist=750.402.381.751.519.448.752.395.56.388.364.895.389.710.382.291.93.926&"; 
        }
		
        var ticket = "ticket=6_bhwbuwdv3_bzqwqs_b_bgeyspdb7qucrmbeidxvabc6bfgscqpjqshduajnfjct29c3nc5ew6d6&";
        var token = "apptoken=cwi4qrk9a5ad37s7rhcmtp86q";

        var tableURL = baseURL + authString + action + query + clist + format + semester + ticket + token;
	//	document.write(tableURL);
	   $.ajax({
            url: tableURL,
            dataType: "script",
            success: function(data, status, xhr){
                $("#loadingString").hide();
				
		//I have to do some stupid hard coding here: 
		qdb_heading[0] = "School Name";
		qdb_heading[1] = "State";
		qdb_heading[2] = "Students";
		qdb_heading[3] = "Grade Range";
		qdb_heading[4] = "Residential Accommodations";
		qdb_heading[5] = "Open Closed Report";
		qdb_heading[6] = "Seats";
		qdb_heading[7] = "Application Form";
		qdb_heading[8] = "School Application Fee";
		qdb_heading[9] = "Required Tests";
		qdb_heading[10] = "Required Score";
		qdb_heading[11] = "Maximum age by Graduation";
		qdb_heading[12] = "(psar) Mat-pak Link";
		qdb_heading[13] = "(psar) GP-Mat-pak Link";
		qdb_heading[14] = "School - (fees) Wire Transfer Recipients Report HTML";
		qdb_heading[15] = "Notes";
		qdb_heading[16] = "(nsar) Admissions Process";
		qdb_heading[17] = "School - Admissions Timeline Compliance";

		//end of hard coding 
		
                window.dataTable = $(tableID).dataTable({
                    "fnInitComplete": function() {
						$(".dataTables_filter > label > input").addClass("form-control");
						$(".dataTables_filter > label > input").attr("placeholder", "Search");
                    },
                    "fnDrawCallback": function() {
                        $(".compare-btn").each(function(idx, btn) {
                            btn = $(btn);
                            if (btn.find("span").hasClass("glyphicon-minus") && !(btn.attr("name") in window.compareFilter)) {
                                btn.html('<span class="glyphicon glyphicon-plus"></span>');
                            }
                        });
                        $(".compare-btn").unbind("click");
                        $(".compare-btn").click(function() {
                            toggleCompareButton($(this));
                        });
                    },
                    "aaData": qdb_data,
                    "aoColumns": $.map(qdb_heading, function(item){
                       return {"sTitle": item.replace( /[(][^)]*[)]/, '' )};
                    }),
                    "iDisplayLength": -1,
                    "aLengthMenu": [[-1, 10, 25, 50], ["All", 10, 25, 50]]
                });
                window.dataTable.fnSort( [ [2,'asc'] ] );
            }
        });        
    };
    
    var destroyTable = function(tableID) {
        window.dataTable.fnDestroy();
        $(tableID).remove();
        window.dataTable = null;
        $("#dataTableContainer").append($("<table id='" + tableID.replace( /^#/, '' ) + "' class='table'></table>"));
        $("#loadingString").show();
    };
    
    window.updateTable = updateTable;
    window.destroyTable = destroyTable;

})(jQuery);