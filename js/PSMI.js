//schema download: https://thecambridgeinstitute.quickbase.com/db/bgg2mdmbk?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&a=API_GetSchema&ticket=6_bhwbuwdv3_bzqwqs_b_bgeyspdb7qucrmbeidxvabc6bfgscqpjqshduajnfjct29c3nc5ew6d6&apptoken=cwi4qrk9a5ad37s7rhcmtp86q

// Most of the main functionalities goes into the jquery closure
(function ($){
    window.hashtag = location.hash.replace( /^#/, '' ) || '';
    window.query = "";
    
    $(window).hashchange( function(){
        window.hashtag = location.hash.replace( /^#/, '' ) || '';
        buildQueryInit();
        destroyTable("#PSMITable");
        updateTable("bgg2mdmbk", window.query, "39", "#PSMITable");
    });
    
    $(document).ready(function () {
        buildQueryInit();
        updateTable("bgg2mdmbk", window.query, "39", "#PSMITable");
        buildTuitionSlider();
        
        bindEventHandlers();
    });
    
    function buildQueryInit() {
        if (window.hashtag !== "") {
            $("#clearSchools").show();
            buildPSMIQuery();
        } else {
            $("#clearSchools").hide();
            window.query = "";
        }
    }
    
    function bindEventHandlers() {
        $("#clearButton").click(function(){
            clearSearch();
        });
        
        $("#searchButton").click(function(){
            window.hashtag = "";
            toggleSearch();
            buildPSMIQuery(); 
            destroyTable("#PSMITable");
            updateTable("bgg2mdmbk", window.query, "39", "#PSMITable");
        });
        
        $("#compareButton").click(function(){
            window.open("psar.html#35/" + $("#compareInput").val());
        });
        
        $("#clearCompare").click(function(){
            $("#compareInput").val("");
            window.compareFilter = {};
            $(".compare-btn").each(function(idx, btn) {
                $(btn).html('<span class="glyphicon glyphicon-plus"></span>');
            });
        });
    }
    
    function buildTuitionSlider() {
        window.slider = $('#tuitionRangeSlider').slider({
            "min": 10000,
            "max": 70000,
            "step": 10000,
            "orientation": "horizontal",
            "value": [20000, 60000],
            "handle": "round"
        });
        window.slider.on("slide", function(e){
            $("#tuitionRangeValue").html($(".tooltip-inner").text());
            jQuery('#clearTuitionRange').show();    jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);    jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);    jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);
            jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);
        });
    }
    
    function clearSearch() {
        $("#stateInput").val("");
        clearTuitionRange();
        $(".checkbox > input").removeAttr("checked");
        window.query = "";
        destroyTable("#PSMITable");
        updateTable("bgg2mdmbk", window.query, "39", "#PSMITable");
    }

})(jQuery);

//functions that gets used by anchor tags
function destroyMap() {
    jQuery("#USMap").html("");
}

function buildMap() {
    jQuery("#USMap").vectorMap({
        map: 'us_aea_en',
        zoomOnScroll: false,
        onRegionClick: function(event, label) {
            //var state = jQuery(".jvectormap-label").text();
            //window.PSMITable.fnFilter(state);
            var state = label.split("-")[1];
            jQuery("#stateInput").val($.trim(jQuery("#stateInput").val() +  " " + state));
        }
    });
}

function toggleSearch() {
    var showSearchOptions = jQuery("#showSearchOptions");
    var searchOptions = jQuery("#searchOptions"); 
    if (showSearchOptions.hasClass("active")) {
        showSearchOptions.removeClass("active");
        searchOptions.hide();
        destroyMap();
    } else {
        showSearchOptions.addClass("active");
        searchOptions.show();
        buildMap();
    }
}

function clearTuitionRange() {
    jQuery('#tuitionRangeValue').html('');
    jQuery('#clearTuitionRange').hide();
    jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);
}