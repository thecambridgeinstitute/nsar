##############################################################################################
#
#   NSAR New WebPage 
#
##############################################################################################

Time: 
	09/19/2014 to 09/24/2014 
Developer: 
	Qiang Sun (qsun@thecambridgeinstitue.org) 

Working directory: 
	server: thecambridgeinstitue.org
	file directory: thecambridgeinstitue.org/cn/n/nsar
	old file: nsar_bak_20140924.php

Changes: 
	1) Based on PSAR template, we change the reports' id from 35/40 to 22/95 (95 is created 22 
	   for Sprint 2015) in index.html 
	2) We change field id list in js/table_builder.js (see clist's new value). 
	3) We hard code the new title for qdb_heading (see line 37 to 57). The new value is read 
	   from quickbase report column name (click to see the property of the column). 
	4) We remove "Compare" and "Search" options from PSAR template. 
	5) We try to change word "PSAR" to "NSAR" but not 100% if such word is used as parameter 
	   in the code. 

Potential Improvement: 
	1) Void hard coding !!!
	2) Create more readable "RREAME.txt" for help future maintaining. 
	3) Replace all PSAR to void any confusion. 
	
Comment: 
	1) Hard coding files: index.html (for report id 22 and 95)
						table_builder.js (clist is the major one). 
	