<?php

$thisfilesname = "nsar.php";

header('Content-type: text/html; charset=utf-8');

// Function definition string contains -- found at 
function strcon($haystack, $needle)
{
	$position = strpos($haystack, $needle);
	if ($position === false) {
		return false;
		}
	else {
		return true;
		}
}

// set the selection of states 

$qs_states = '{479.EX.XX}';

if (strcon($_GET["s"],'AL')) {$qs_states .= 'OR{479.EX.AL}';}
if (strcon($_GET["s"],'AK')) {$qs_states .= 'OR{479.EX.AK}';}
if (strcon($_GET["s"],'AZ')) {$qs_states .= 'OR{479.EX.AZ}';}
if (strcon($_GET["s"],'AR')) {$qs_states .= 'OR{479.EX.AR}';}
if (strcon($_GET["s"],'CA')) {$qs_states .= 'OR{479.EX.CA}';}
if (strcon($_GET["s"],'CO')) {$qs_states .= 'OR{479.EX.CO}';}
if (strcon($_GET["s"],'CT')) {$qs_states .= 'OR{479.EX.CT}';}
if (strcon($_GET["s"],'DE')) {$qs_states .= 'OR{479.EX.DE}';}
if (strcon($_GET["s"],'DC')) {$qs_states .= 'OR{479.EX.DC}';}
if (strcon($_GET["s"],'FL')) {$qs_states .= 'OR{479.EX.FL}';}
if (strcon($_GET["s"],'GA')) {$qs_states .= 'OR{479.EX.GA}';}
if (strcon($_GET["s"],'HI')) {$qs_states .= 'OR{479.EX.HI}';}
if (strcon($_GET["s"],'ID')) {$qs_states .= 'OR{479.EX.ID}';}
if (strcon($_GET["s"],'IL')) {$qs_states .= 'OR{479.EX.IL}';}
if (strcon($_GET["s"],'IN')) {$qs_states .= 'OR{479.EX.IN}';}
if (strcon($_GET["s"],'IA')) {$qs_states .= 'OR{479.EX.IA}';}
if (strcon($_GET["s"],'KS')) {$qs_states .= 'OR{479.EX.KS}';}
if (strcon($_GET["s"],'KY')) {$qs_states .= 'OR{479.EX.KY}';}
if (strcon($_GET["s"],'LA')) {$qs_states .= 'OR{479.EX.LA}';}
if (strcon($_GET["s"],'ME')) {$qs_states .= 'OR{479.EX.ME}';}
if (strcon($_GET["s"],'MD')) {$qs_states .= 'OR{479.EX.MD}';}
if (strcon($_GET["s"],'MA')) {$qs_states .= 'OR{479.EX.MA}';}
if (strcon($_GET["s"],'MI')) {$qs_states .= 'OR{479.EX.MI}';}
if (strcon($_GET["s"],'MN')) {$qs_states .= 'OR{479.EX.MN}';}
if (strcon($_GET["s"],'MS')) {$qs_states .= 'OR{479.EX.MS}';}
if (strcon($_GET["s"],'MO')) {$qs_states .= 'OR{479.EX.MO}';}
if (strcon($_GET["s"],'MT')) {$qs_states .= 'OR{479.EX.MT}';}
if (strcon($_GET["s"],'NE')) {$qs_states .= 'OR{479.EX.NE}';}
if (strcon($_GET["s"],'NV')) {$qs_states .= 'OR{479.EX.NV}';}
if (strcon($_GET["s"],'NH')) {$qs_states .= 'OR{479.EX.NH}';}
if (strcon($_GET["s"],'NJ')) {$qs_states .= 'OR{479.EX.NJ}';}
if (strcon($_GET["s"],'NM')) {$qs_states .= 'OR{479.EX.NM}';}
if (strcon($_GET["s"],'NY')) {$qs_states .= 'OR{479.EX.NY}';}
if (strcon($_GET["s"],'NC')) {$qs_states .= 'OR{479.EX.NC}';}
if (strcon($_GET["s"],'ND')) {$qs_states .= 'OR{479.EX.ND}';}
if (strcon($_GET["s"],'OH')) {$qs_states .= 'OR{479.EX.OH}';}
if (strcon($_GET["s"],'OK')) {$qs_states .= 'OR{479.EX.OK}';}
if (strcon($_GET["s"],'OR')) {$qs_states .= 'OR{479.EX.OR}';}
if (strcon($_GET["s"],'PA')) {$qs_states .= 'OR{479.EX.PA}';}
if (strcon($_GET["s"],'PR')) {$qs_states .= 'OR{479.EX.PR}';}
if (strcon($_GET["s"],'RI')) {$qs_states .= 'OR{479.EX.RI}';}
if (strcon($_GET["s"],'SC')) {$qs_states .= 'OR{479.EX.SC}';}
if (strcon($_GET["s"],'SD')) {$qs_states .= 'OR{479.EX.SD}';}
if (strcon($_GET["s"],'TN')) {$qs_states .= 'OR{479.EX.TN}';}
if (strcon($_GET["s"],'TX')) {$qs_states .= 'OR{479.EX.TX}';}
if (strcon($_GET["s"],'UT')) {$qs_states .= 'OR{479.EX.UT}';}
if (strcon($_GET["s"],'VT')) {$qs_states .= 'OR{479.EX.VT}';}
if (strcon($_GET["s"],'VA')) {$qs_states .= 'OR{479.EX.VA}';}
if (strcon($_GET["s"],'WA')) {$qs_states .= 'OR{479.EX.WA}';}
if (strcon($_GET["s"],'WV')) {$qs_states .= 'OR{479.EX.WV}';}
if (strcon($_GET["s"],'WI')) {$qs_states .= 'OR{479.EX.WI}';}
if (strcon($_GET["s"],'WY')) {$qs_states .= 'OR{479.EX.WY}';}

if ($qs_states == '{479.EX.XX}')
	$qs_states = '{528.EX.1}';   // if no $qs_states, set it to a contsraint that is always true.
else
	$qs_states = '('.$qs_states.')';

 //echo $qs_states.'<br>';

// set the selection of tuitions

$qs_tuition = '{479.EX.XX}';

$tuition_field_number = 253;
$tif = $tuition_field_number;

if ($_GET["20k"]=='30k') {$qs_tuition .= 'OR({'.$tif.'.GTE.20000}AND{'.$tif.'.LTE.30000})';}
if ($_GET["30k"]=='40k') {$qs_tuition .= 'OR({'.$tif.'.GTE.30000}AND{'.$tif.'.LTE.40000})';}
if ($_GET["40k"]=='50k') {$qs_tuition .= 'OR({'.$tif.'.GTE.40000}AND{'.$tif.'.LTE.50000})';}
if ($_GET["50k"]=='60k') {$qs_tuition .= 'OR({'.$tif.'.GTE.50000}AND{'.$tif.'.LTE.60000})';}
if ($_GET["60k"]=='70k') {$qs_tuition .= 'OR({'.$tif.'.GTE.60000}AND{'.$tif.'.LTE.70000})';}
	
if ($qs_tuition == '{479.EX.XX}')
	$qs_tuition = '{528.EX.1}';   // if no $qs_tuition, set it to a contsraint that is always true.
else
	$qs_tuition = '('.$qs_tuition.')';

// echo $qs_tuition.'<br>';

// collect qs variables into querystring

$querystring = '(' . $qs_tuition . 'AND' . $qs_states . 'AND{528.EX.1})';

// $querystring = '({528.EX.1})';

$qs_if_no_query = '({528.EX.1}AND{528.EX.1}AND{528.EX.1})';
if($querystring==$qs_if_no_query)
  

// fields	
	
$fields = '750.402.381.751.519.448.752.395.394.749.753.291';
		
?>
<html><head>
<?php
$url='https://www.quickbase.com/db/main?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&hours=24';
$outputfile="/home/cicbr/thecambridgeinstitute.org/cn/n/ticket1.xml";
$cmd="wget -q \"$url\" -O $outputfile";
exec($cmd);
$xml=file_get_contents($outputfile);
$tickettag="<ticket>";
$tickettag1="</ticket>";
$beginpos=strpos($xml,$tickettag);
$endpos=strpos($xml,$tickettag1);
//echo $beginpos, $endpos;
$ticket=substr($xml,$beginpos+8,$endpos-$beginpos-8);

$dbid = "bhebcy4n5"; // SR Plans
$qid = "22"; // Fall 2013
$fields = '750.402.381.751.519.448.752.395.394.749.753.291';
//echo $fields;
//echo 'https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?a=API_GenResultsTable&query='.$querystring.'&clist='.$fields;
if ($querystring == $qs_if_no_query )
       echo '<script lang="javascript" src="https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&a=API_GenResultsTable&qid=22&ticket='.$ticket.'&jht=1&apptoken=cwi4qrk9a5ad37s7rhcmtp86q"></script>';
      
else
     echo '<script lang="javascript" src="https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&a=API_GenResultsTable&query=' . $querystring .'&clist='. $fields .'&ticket='.$ticket.'&slist=518.75&opts=so-AA.gb-VX.nos.&jht=1&apptoken=cwi4qrk9a5ad37s7rhcmtp86q"></script>';

  //  echo '<script lang="javascript" src="https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?a=API_GenResultsTable&query=' . $querystring .'&clist='. $fields .'&jht=1&apptoken=cwi4qrk9a5ad37s7rhcmtp86q"></script>';
     // td.hd { font-family:verdana; font-size:77%; font-weight:bold; color:white; }

?>
<?php //src="https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?a=API_GenResultsTable&qid=22&jht=1&apptoken=cwi4qrk9a5ad37s7rhcmtp86q"?>
<script language="javascript">
    function getItem(id)
    {
        var itm = false;
        if(document.getElementById)
            itm = document.getElementById(id);
        else if(document.all)
            itm = document.all[id];
        else if(document.layers)
            itm = document.layers[id];

        return itm;
    }

    function toggleItem(id)
    {
        itm = getItem(id);

        if(!itm)
            return false;

        if(itm.style.display == 'none')
            itm.style.display = '';
        else
            itm.style.display = 'none';

        return false;
    }
    </script>

</head>

<style>
td.m { background-color:#ececec; padding:0px; font-family:verdana; font-size:70%; }
td.hd { font-family:verdana; font-size:77%; font-weight:bold; color:white; }
td.ev { background-color:#ececec}

tr.ev { background-color:white; bgcolor:white }
<?php

if ($querystring == $qs_if_no_query )
	echo 'td.hd { font-family:verdana; font-size:100%; padding:4px; font-weight:bold; color:white; background-color:#555555; text-align:center;}';
else
	echo 'td.hd { font-family:verdana; font-size:1%; color:white; background-color:#ffffff; text-align:center;}';

?>

div.fill { height:100% }

/* Font Definitions */
 @font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:PMingLiU;
	panose-1:2 2 5 0 0 0 0 0 0 0;}
@font-face
	{font-family:PMingLiU;
	panose-1:2 2 5 0 0 0 0 0 0 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"\@PMingLiU";
	panose-1:2 2 5 0 0 0 0 0 0 0;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"Berling Becker";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:9.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:24.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	line-height:115%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Berling Becker";
	color:#365F91;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:10.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	line-height:115%;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Berling Becker";
	color:#4F81BD;}
h3
	{mso-style-link:"Heading 3 Char";
	margin-top:10.0pt;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:0in;
	margin-bottom:.0001pt;
	line-height:115%;
	page-break-after:avoid;
	font-size:13.0pt;
	font-family:"Berling Becker";
	color:#4F81BD;
	font-weight:normal;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Title Char";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:15.0pt;
	margin-left:0in;
	border:none;
	padding:0in;
	font-size:26.0pt;
	font-family:"Berling Becker";
	color:#17365D;
	letter-spacing:.25pt;
	font-weight:bold;}
p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
	{mso-style-link:"Title Char";
	margin:0in;
	margin-bottom:.0001pt;
	border:none;
	padding:0in;
	font-size:26.0pt;
	font-family:"Berling Becker";
	color:#17365D;
	letter-spacing:.25pt;
	font-weight:bold;}
p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
	{mso-style-link:"Title Char";
	margin:0in;
	margin-bottom:.0001pt;
	border:none;
	padding:0in;
	font-size:26.0pt;
	font-family:"Berling Becker";
	color:#17365D;
	letter-spacing:.25pt;
	font-weight:bold;}
p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
	{mso-style-link:"Title Char";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:15.0pt;
	margin-left:0in;
	border:none;
	padding:0in;
	font-size:26.0pt;
	font-family:"Berling Becker";
	color:#17365D;
	letter-spacing:.25pt;
	font-weight:bold;}
p.MsoNoSpacing, li.MsoNoSpacing, div.MsoNoSpacing
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:9.0pt;
	margin-left:.5in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	margin-bottom:.0001pt;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:9.0pt;
	margin-left:.5in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Tahoma","sans-serif";}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-link:Title;
	font-family:"Berling Becker";
	color:#17365D;
	letter-spacing:.25pt;
	font-weight:bold;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Berling Becker";
	color:#365F91;
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Berling Becker";
	color:#4F81BD;
	font-weight:bold;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-link:"Heading 3";
	font-family:"Berling Becker";
	color:#4F81BD;}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
</style>

<body>

<div align=center class=WordSection1>

<div align=center style='border:none;border-bottom:solid #4F81BD 1.0pt;padding:0in 0in 4.0pt 0in'>

<p class=MsoTitle>CIIE Non-Exclusive Partner Admission Requirements</p>

</div>
<br>
<?php 
if ($querystring == $qs_if_no_query ) {
echo <<<END

<script type="text/javascript">
function addMsg(text,element_id) {
//alert (document.getElementById(element_id).value);
if (document.getElementById(element_id).value.indexOf(text) < 0) {
document.getElementById(element_id).value += text;
}
} 
</script>


    <tbody>
        <tr><a href="#" onclick="toggleItem('myTbody')"><font size="5">Show Search Options >>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></a></td></tr>
    </tbody>
<span style="font-size:80%;"><br>&nbsp;<br></span>
  <form action="./
END;

echo $thisfilesname;

echo <<<ENE
" method="get">
      <div align="center">
<table border="1px"><tr><td>
  <table width="679" height="334" border="0px" cellpadding="5px">

    <tbody id="myTbody">
    <tr>
      <td width="507" height="328">
        <div style="height: 327px; width: 501px; ">
          <map name="ImageMap_1_1634522264" id="ImageMap_1_1634522264">
<area href="javascript:void(0)" onclick="addMsg('WA ','id_states'); return false;" shape="poly" coords="62,5,102,15,96,47,81,44,69,44,61,42,54,42,52,35,46,32,46,9" alt="华盛顿 Washington (WA)" title="华盛顿 Washington (WA)" />
<area href="javascript:void(0)" onclick="addMsg('OR ','id_states'); return false;" shape="poly" coords="45,32,24,70,29,75,35,77,63,85,87,90,92,67,91,65,91,60,94,60,99,51,97,47,84,44,69,44,61,43,54,42,52,36" alt="俄勒冈州 Oregon (OR)" title="俄勒冈州 Oregon (OR)" />
<area href="javascript:void(0)" onclick="addMsg('CA ','id_states'); return false;" shape="poly" coords="27,75,15,102,30,162,43,191,86,195,86,187,91,182,93,177,54,116,62,85" alt="加州 California (CA)" title="加州 California (CA)" />
<area href="javascript:void(0)" onclick="addMsg('HI ','id_states'); return false;" shape="poly" coords="4,227,75,227,76,279,3,278" alt="夏威夷 Hawaii (HI)" title="夏威夷 Hawaii (HI)" />
<area href="javascript:void(0)" onclick="addMsg('AK ','id_states'); return false;" shape="poly" coords="78,227,151,227,196,295,197,323,4,324,4,281,77,282" alt="阿拉斯加州 Alaska (AK)" title="阿拉斯加州 Alaska (AK)" />
<area href="javascript:void(0)" onclick="addMsg('ID ','id_states'); return false;" shape="poly" coords="111,17,103,16,96,47,99,52,94,61,91,65,86,90,133,99,138,71,136,68,133,69,123,70,118,55,114,55,118,44,116,44,110,35,107,25" alt="爱达荷州 Idaho (ID)" title="爱达荷州 Idaho (ID)" />
<area href="javascript:void(0)" onclick="addMsg('VT ','id_states'); return false;" shape="circle" coords="403,30,10" alt="佛蒙特 Vermont (VT)" title="佛蒙特 Vermont (VT)" />
<area href="javascript:void(0)" onclick="addMsg('NH ','id_states'); return false;" shape="circle" coords="428,30,11" alt="新罕布什尔州 New Hampshire (NH)" title="新罕布什尔州 New Hampshire (NH)" />
<area href="javascript:void(0)" onclick="addMsg('MA ','id_states'); return false;" shape="circle" coords="484,72,11" alt="麻州 Massachusetts (MA)" title="麻州 Massachusetts (MA)" />
<area href="javascript:void(0)" onclick="addMsg('RI ','id_states'); return false;" shape="circle" coords="484,94,9" alt="罗德岛 Rhode Island (RI)" title="罗德岛 Rhode Island (RI)" />
<area href="javascript:void(0)" onclick="addMsg('CT ','id_states'); return false;" shape="circle" coords="484,117,10" alt="康州 Connecticut (CT)" title="康州 Connecticut (CT)" />
<area href="javascript:void(0)" onclick="addMsg('NJ ','id_states'); return false;" shape="circle" coords="485,137,10" alt="新泽西 New Jersey (NJ)" title="新泽西 New Jersey (NJ)" />
<area href="javascript:void(0)" onclick="addMsg('DE ','id_states'); return false;" shape="circle" coords="485,159,10" alt="特拉华州 Delaware (DE)" title="特拉华州 Delaware (DE)" />
<area href="javascript:void(0)" onclick="addMsg('MD ','id_states'); return false;" shape="circle" coords="485,182,11" alt="马里兰 Maryland (MD)" title="马里兰 Maryland (MD)" />
<area href="javascript:void(0)" onclick="addMsg('DC ','id_states'); return false;" shape="circle" coords="484,202,9" alt="华盛顿特区 Washington, D.C. (DC)" title="华盛顿特区 Washington, D.C. (DC)" />
<area href="javascript:void(0)" onclick="addMsg('PR ','id_states'); return false;" shape="circle" coords="473,290,10" alt="波多黎各 Puerto Rico (PR)" title="波多黎各 Puerto Rico (PR)" />
<area href="javascript:void(0)" onclick="addMsg('MT ','id_states'); return false;" shape="poly" coords="196,29,157,25,109,17,108,27,110,37,117,45,114,55,119,54,122,71,137,70,139,66,192,73" alt="蒙大拿 Montana (MT)" title="蒙大拿 Montana (MT)" />
<area href="javascript:void(0)" onclick="addMsg('ND ','id_states'); return false;" shape="poly" coords="245,31,198,30,194,63,250,65,250,59" alt="北达科他州 North Dakota (ND)" title="北达科他州 North Dakota (ND)" />
<area href="javascript:void(0)" onclick="addMsg('SD ','id_states'); return false;" shape="poly" coords="252,103,252,72,248,68,251,65,194,62,191,95,236,98,239,101,241,99,246,99" alt="南达科他州 South Dakota (SD)" title="南达科他州 South Dakota (SD)" />
<area href="javascript:void(0)" onclick="addMsg('NV ','id_states'); return false;" shape="poly" coords="64,86,110,95,97,160,93,158,91,169,55,116" alt="内华达州 Nevada (NV)" title="内华达州 Nevada (NV)" />
<area href="javascript:void(0)" onclick="addMsg('UT ','id_states'); return false;" shape="poly" coords="110,95,133,99,132,109,148,111,142,157,100,149" alt="犹他州 Utah (UT)" title="犹他州 Utah (UT)" />
<area href="javascript:void(0)" onclick="addMsg('AZ ','id_states'); return false;" shape="poly" coords="142,155,99,149,98,156,95,159,91,158,93,175,86,194,84,197,115,215,134,218" alt="阿肯色州 Arkansas (AR)" title="阿肯色州 Arkansas (AR)" />
<area href="javascript:void(0)" onclick="addMsg('WY ','id_states'); return false;" shape="poly" coords="193,73,138,67,132,109,190,116" alt="怀俄明州 Wyoming (WY)" title="怀俄明州 Wyoming (WY)" />
<area href="javascript:void(0)" onclick="addMsg('CO ','id_states'); return false;" shape="poly" coords="205,118,148,112,143,155,203,162" alt="科州 Colorado (CO)" title="科州 Colorado (CO)" />
<area href="javascript:void(0)" onclick="addMsg('NM ','id_states'); return false;" shape="poly" coords="144,156,135,217,142,217,144,213,158,214,159,212,191,216,196,163" alt="新墨西哥 New Mexico (NM)" title="新墨西哥 New Mexico (NM)" />
<area href="javascript:void(0)" onclick="addMsg('NE ','id_states'); return false;" shape="poly" coords="251,103,247,99,239,99,235,97,191,96,189,116,205,118,204,129,261,130,257,124" alt="内布拉斯加州 Nebraska (NE)" title="内布拉斯加州 Nebraska (NE)" />
<area href="javascript:void(0)" onclick="addMsg('KS ','id_states'); return false;" shape="poly" coords="262,130,206,130,203,162,266,163,267,139,263,136,266,133" alt="堪州 Kansas (KS)" title="堪州 Kansas (KS)" />
<area href="javascript:void(0)" onclick="addMsg('OK ','id_states'); return false;" shape="poly" coords="195,162,195,167,220,167,220,189,231,195,236,195,240,198,251,198,263,198,270,201,268,176,268,164" alt="俄克拉何马 Oklahoma (OK)" title="俄克拉何马 Oklahoma (OK)" />
<area href="javascript:void(0)" onclick="addMsg('TX ','id_states'); return false;" shape="poly" coords="243,284,225,279,220,264,209,246,204,241,196,240,191,242,187,249,178,245,175,242,170,228,158,215,158,213,190,217,195,168,219,169,219,189,228,193,236,196,243,196,262,197,269,200,273,201,275,218,279,230,277,244" alt="得州 Texas (TX)" title="得州 Texas (TX)" />
<area href="javascript:void(0)" onclick="addMsg('MN ','id_states'); return false;" shape="poly" coords="302,40,291,39,288,40,279,36,267,35,262,33,246,32,251,63,248,69,253,74,252,91,294,91,291,85,280,78,280,70,279,67,282,62,283,54" alt="明尼苏达州 Minnesota (MN)" title="明尼苏达州 Minnesota (MN)" />
<area href="javascript:void(0)" onclick="addMsg('IA ','id_states'); return false;" shape="poly" coords="293,92,251,92,251,101,258,123,291,123,292,125,297,120,296,113,301,112,303,106,298,100,295,100" alt="爱荷华州 Iowa (IA)" title="爱荷华州 Iowa (IA)" />
<area href="javascript:void(0)" onclick="addMsg('MO ','id_states'); return false;" shape="poly" coords="311,172,313,165,314,166,315,160,313,160,310,153,303,148,305,141,300,140,300,136,293,129,293,123,258,124,262,131,265,132,262,136,266,140,268,169,305,168,306,172" alt="密苏里州 Missouri (MO)" title="密苏里州 Missouri (MO)" />
<area href="javascript:void(0)" onclick="addMsg('AR ','id_states'); return false;" shape="poly" coords="308,183,312,173,306,173,308,167,268,170,269,201,273,201,273,207,300,206,300,202,299,198,311,173" alt="亚利桑那 Arizona (AZ)" title="亚利桑那 Arizona (AZ)" />
<area href="javascript:void(0)" onclick="addMsg('LA ','id_states'); return false;" shape="poly" coords="314,233,319,238,327,248,320,252,277,244,278,228,275,220,272,207,300,207,303,216,298,228,313,228" alt="路易斯安那州 Louisiana (LA)" title="路易斯安那州 Louisiana (LA)" />
<area href="javascript:void(0)" onclick="addMsg('MI ','id_states'); return false;" shape="poly" coords="356,104,363,90,352,65,345,53,316,45,299,56,302,60,316,63,320,70,322,71,326,67,331,105" alt="密歇根 Michigan (MI)" title="密歇根 Michigan (MI)" />
<area href="javascript:void(0)" onclick="addMsg('WV ','id_states'); return false;" shape="poly" coords="383,112,382,125,376,129,375,133,373,133,372,137,370,140,370,143,378,151,381,151,381,150,384,150,390,147,392,141,394,135,398,135,400,130,403,126,403,123,405,123,406,124,408,124,408,122,406,120,403,121,395,126,393,121,393,121,389,123,385,123" alt="西弗吉尼亚州 West Virginia (WV)" title="西弗吉尼亚州 West Virginia (WV)" />
<area href="javascript:void(0)" onclick="addMsg('IL ','id_states'); return false;" shape="poly" coords="325,109,326,137,328,141,324,151,323,157,320,156,321,160,315,159,314,161,311,161,312,153,303,148,305,141,299,139,299,135,292,127,296,118,296,115,301,111,302,106,300,102,298,100,321,100" alt="伊利诺州 Illinois (IL)" title="伊利诺州 Illinois (IL)" />
<area href="javascript:void(0)" onclick="addMsg('IN ','id_states'); return false;" shape="poly" coords="346,104,331,107,323,109,327,138,324,151,336,151,338,146,341,149,345,140,350,138" alt="印地安那 Indiana (IN)" title="印地安那 Indiana (IN)" />
<area href="javascript:void(0)" onclick="addMsg('OH ','id_states'); return false;" shape="poly" coords="380,97,371,105,364,107,360,105,346,106,350,134,354,135,358,138,367,137,371,140,373,134,375,134,376,130,382,125,382,111" alt="俄亥俄州 Ohio (OH)" title="俄亥俄州 Ohio (OH)" />
<area href="javascript:void(0)" onclick="addMsg('WI ','id_states'); return false;" shape="poly" coords="299,56,294,57,294,54,284,57,283,64,279,69,281,77,293,87,296,99,301,102,320,100,325,69,321,71,318,66,318,64,301,59" alt="威斯康星州 Wisconsin (WI)" title="威斯康星州 Wisconsin (WI)" /><area href="javascript:void(0)" onclick="addMsg('KY ','id_states'); return false;" shape="poly" coords="370,143,370,140,366,137,363,138,358,138,355,136,350,134,350,137,345,140,341,148,338,146,336,150,324,151,323,156,320,156,320,159,316,159,315,165,313,167,321,167,325,165,333,164,343,163,351,162,362,162,365,160,370,156,373,153,376,150" alt="肯塔基州 Kentucky (KY)" title="肯塔基州 Kentucky (KY)" />
<area href="javascript:void(0)" onclick="addMsg('TN ','id_states'); return false;" shape="poly" coords="381,158,372,159,372,160,364,160,364,161,356,162,326,165,325,166,313,167,312,174,308,184,324,183,326,182,360,179,360,176,373,167,381,161" alt="田纳西州 Tennessee (TN)" title="田纳西州 Tennessee (TN)" />
<area href="javascript:void(0)" onclick="addMsg('MS ','id_states'); return false;" shape="poly" coords="326,183,322,183,315,184,306,184,300,196,300,207,302,214,298,224,297,228,310,228,311,227,314,227,315,233,317,236,328,235,327,226,326,219" alt="密西西比河 Mississippi (MS)" title="密西西比河 Mississippi (MS)" />
<area href="javascript:void(0)" onclick="addMsg('AL ','id_states'); return false;" shape="poly" coords="349,181,326,182,326,219,328,234,332,238,340,234,335,225,359,224,358,217,357,207,348,181,326,182" alt="阿拉巴马州 Alabama (AL)" title="阿拉巴马州 Alabama (AL)" />
<area href="javascript:void(0)" onclick="addMsg('GA ','id_states'); return false;" shape="poly" coords="369,178,363,179,360,179,355,179,354,180,348,180,357,207,359,223,361,226,384,225,386,227,388,227,387,222,393,222,395,206,388,197,374,184,369,182" alt="格鲁吉亚 Georgia (GA)" title="格鲁吉亚 Georgia (GA)" />
<area href="javascript:void(0)" onclick="addMsg('FL ','id_states'); return false;" shape="poly" coords="395,222,387,222,387,226,385,227,385,225,367,226,360,226,358,223,354,224,347,225,335,226,340,234,360,241,380,244,385,267,400,281,392,292,400,293,423,281,412,248" alt="佛州 Florida (FL)" title="佛州 Florida (FL)" />
<area href="javascript:void(0)" onclick="addMsg('SC ','id_states'); return false;" shape="poly" coords="413,184,402,175,397,175,397,176,392,176,389,173,380,173,369,178,369,182,374,184,388,198,393,206,397,206,414,190" alt="南卡州 South Carolina (SC)" title="南卡州 South Carolina (SC)" />
<area href="javascript:void(0)" onclick="addMsg('NC ','id_states'); return false;" shape="poly" coords="411,154,404,155,387,158,381,158,380,161,372,167,362,174,359,177,359,179,364,179,369,178,378,174,385,173,389,173,392,176,397,176,397,175,402,175,413,183,416,186,440,164,434,151,429,150,420,152" alt="北卡州 North Carolina (NC)" title="北卡州 North Carolina (NC)" />
<area href="javascript:void(0)" onclick="addMsg('VA ','id_states'); return false;" shape="poly" coords="420,134,416,133,414,132,414,129,415,129,415,126,412,125,410,124,408,123,408,124,405,124,404,123,403,124,403,126,399,131,398,134,394,135,390,147,384,150,381,150,380,151,378,151,376,149,370,154,370,156,364,160,370,160,374,159,380,158,388,158,411,154,429,150,432,148,436,133,433,131,428,134" alt="弗吉尼亚 Virginia (VA)" title="弗吉尼亚 Virginia (VA)" />
<area href="javascript:void(0)" onclick="addMsg('MD ','id_states'); return false;" shape="poly" coords="423,115,416,116,415,117,411,118,406,120,410,124,413,125,414,123,416,124,415,126,414,132,419,134,422,134,428,134,436,129,428,129,425,122,423,116" alt="马里兰 Maryland (MD)" title="马里兰 Maryland (MD)" />
<area href="javascript:void(0)" onclick="addMsg('MD ','id_states'); return false;" shape="poly" coords="405,120,393,121,394,125,395,126" alt="马里兰 Maryland (MD)" title="马里兰 Maryland (MD)" />
<area href="javascript:void(0)" onclick="addMsg('DC ','id_states'); return false;" shape="poly" coords="413,123,413,126,416,126,416,124,414,123" alt="华盛顿特区 Washington, D.C. (DC)" title="华盛顿特区 Washington, D.C. (DC)" />
<area href="javascript:void(0)" onclick="addMsg('DE ','id_states'); return false;" shape="poly" coords="426,114,423,115,427,128,436,129,428,120" alt="特拉华州 Delaware (DE)" title="特拉华州 Delaware (DE)" />
<area href="javascript:void(0)" onclick="addMsg('NY ','id_states'); return false;" shape="poly" coords="452,95,441,104,436,102,435,98,427,96,422,90,387,97,385,95,391,90,392,87,389,83,394,80,402,80,404,79,410,76,409,68,412,65,416,59,418,57,429,54,431,61,433,69,436,78,436,90,437,97,439,98,448,94" alt="纽约 New York (NY)" title="纽约 New York (NY)" />
<area href="javascript:void(0)" onclick="addMsg('PA ','id_states'); return false;" shape="poly" coords="424,114,421,116,416,116,407,119,401,120,394,121,391,122,386,122,385,116,382,99,386,95,388,97,422,90,427,96,426,105,430,109,428,112,427,114" alt="宾州 Pennsylvania (PA)" title="宾州 Pennsylvania (PA)" />
<area href="javascript:void(0)" onclick="addMsg('NJ ','id_states'); return false;" shape="poly" coords="427,96,426,104,430,108,430,110,426,114,427,118,428,120,432,124,441,117,441,105,436,102,435,98" alt="新泽西 New Jersey (NJ)" title="新泽西 New Jersey (NJ)" />
<area href="javascript:void(0)" onclick="addMsg('CT ','id_states'); return false;" shape="poly" coords="452,91,451,88,450,86,450,83,448,83,446,84,442,85,437,85,437,89,438,94,437,96,439,98,441,96,445,94" alt="康州 Connecticut (CT)" title="康州 Connecticut (CT)" />
<area href="javascript:void(0)" onclick="addMsg('RI ','id_states'); return false;" shape="poly" coords="452,90,455,91,454,86,453,82,450,83,451,88" alt="罗德岛 Rhode Island (RI)" title="罗德岛 Rhode Island (RI)" />
<area href="javascript:void(0)" onclick="addMsg('MA ','id_states'); return false;" shape="poly" coords="458,72,452,73,452,74,447,76,441,77,436,79,436,86,436,89,438,85,444,88,448,83,450,83,452,82,457,90,468,84,467,78" alt="麻州 Massachusetts (MA)" title="麻州 Massachusetts (MA)" />
<area href="javascript:void(0)" onclick="addMsg('NH ','id_states'); return false;" shape="poly" coords="455,69,455,73,452,73,452,74,447,75,448,76,441,76,435,62,442,64,442,62,441,61,441,59,443,59,443,51,443,47,446,47,452,66,456,69" alt="新罕布什尔州 New Hampshire (NH)" title="新罕布什尔州 New Hampshire (NH)" />
<area href="javascript:void(0)" onclick="addMsg('VT ','id_states'); return false;" shape="poly" coords="443,51,439,51,436,52,429,54,431,65,434,72,436,79,447,72,441,65,442,64,442,61,441,60,441,59,443,59,443,57,444,56,444,54" alt="佛蒙特 Vermont (VT)" title="佛蒙特 Vermont (VT)" />
<area href="javascript:void(0)" onclick="addMsg('ME ','id_states'); return false;" shape="poly" coords="455,68,458,68,479,46,477,39,465,19,452,19,448,32,447,42,446,47,450,61" alt="缅因州 Maine (ME)" title="缅因州 Maine (ME)" />
          </map>
          <img onload="toggleItem('myTbody')" src="usa.png" alt="US state abbrev map.png" width="501" height="327" align="middle" usemap="#ImageMap_1_1634522264" />
          <p><br/>
          </p>
          <div style="margin-left: 0px; margin-top: -20px; text-align: left;"></div>
        </div>

      </td>
      <td width="448" VALIGN="top"><p>States chosen:</p>
  <input id="id_states" name="s" type="text" width="40" />
  <p>Tuition Filtering:</p>
        <input type="checkbox" name="20k" value="30k"/>
        $20,000 - $30,000 <br/>
  <input type="checkbox" name="30k" value="40k"/>
        $30,000 - $40,000 <br/>
  <input type="checkbox" name="40k" value="50k"/>
        $40,000 - $50,000 <br/>
  <input type="checkbox" name="50k" value="60k"/>
        $50,000 - $60,000 <br/>
  <input type="checkbox" name="60k" value="70k"/>
      $60,000 - $70,000 <br/>
            <br><br>  
   <span style="float:right;"><input type="reset" value="Clear" align="bottom"/>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<br><br><br>
   <span style="float:right;"><input type="submit" value="Submit" align="bottom"/>&nbsp;&nbsp;&nbsp;&nbsp;</span>
    
 
    </td>
    </tr>
  </tbody>
  </table>
  </tr></td></table>
  </form>
ENE;
}

?>

<p class=MsoNormal>&nbsp;</p>

<!-- <table align=center class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 align=left
 width=613 style='width:460.1pt;border-collapse:collapse;border:none;
 margin-left:6.75pt;margin-right:6.75pt'>
 <tr style='height:45.2pt'>
  <td width=220px style='padding:0in 5.4pt 0in 5.4pt;height:45.2pt'>
  <p class=MsoNoSpacing><span style='font-size:16.0pt'>Key</span></p>
  <p class=MsoNoSpacing><span lang=ZH-TW style='font-size:16.0pt;font-family:
  "PMingLiU","serif"'>&#22270;&#20363;</span></p>
  <td width=220px style='background:#FFCC66;padding:0in 5.4pt 0in 5.4pt;
  height:45.2pt'>
  <p class=MsoNoSpacing align=center style='text-align:center'>Minimum SLEP</p>
  <p class=MsoNoSpacing align=center style='text-align:center'><span
  lang=ZH-CN style='font-family:SimSun'>&#38754;&#35797;</span><span
  lang=ZH-TW style='font-family:"PMingLiU","serif"'>&#26465;&#20214;</span><span
  lang=ZH-CN style='font-family:SimSun'>&#65292;</span><span style='font-family:
  "PMingLiU","serif"'>SLEP</span><br><span lang=ZH-CN style='font-family:SimSun'>&#35201;&#24471;&#21040;&#29305;&#23450;&#26631;&#20934;</span></p>
  </td>
 </tr>
 <tr style='height:30.15pt'>
  <td width=220px style='width:150pt;background:#b1e0f8;padding:0in 5.4pt 0in 5.4pt;
  height:30.15pt'>
  <p class=MsoNoSpacing align=center style='text-align:center'>SSAT required</p>
  <p class=MsoNoSpacing align=center style='text-align:center'><span
  lang=ZH-CN style='font-family:SimSun'>&#24517;&#39035;&#32771;</span><span
  style='font-family:"PMingLiU","serif"'>SSAT</span><span lang=ZH-CN
  style='font-family:SimSun'>&#65292;</span></p>
  <p class=MsoNoSpacing align=center style='text-align:center'><span
  lang=ZH-TW style='font-family:"PMingLiU","serif"'>&#21542;&#21017;&#35201;&#25552;&#21069;&#25910;&#21040;&#25209;&#20934;</span></p>
  <td width=220px style='background:#6FFF6F;padding:0in 5.4pt 0in 5.4pt;
  height:30.15pt'>
  <p class=MsoNoSpacing align=center style='text-align:center'>Minimum TOEFL</p>
  <p class=MsoNoSpacing align=center style='text-align:center'><span
  lang=ZH-CN style='font-family:SimSun'>&#38754;&#35797;</span><span
  lang=ZH-TW style='font-family:"PMingLiU","serif"'>&#26465;&#20214;</span><span
  lang=ZH-CN style='font-family:SimSun'>&#65292;</span><span style='font-family:
  "PMingLiU","serif"'>TOEFL</span><br><span lang=ZH-CN style='font-family:SimSun'>&#35201;&#24471;&#21040;&#29305;&#23450;&#26631;&#20934;</span></p>
  </td>
 </tr>
</table> -->

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>
<?php

if ($querystring != $qs_if_no_query) {

echo "<div style='text-align:left; background-color:#dddddd'>";
echo "<br><b>Filter:</b>";

if($_GET["s"]!='') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Showing states "<i>'.$_GET["s"].'</i>"</b>';}

if($_GET["20k"]=='30k') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tuition from $20,000 to $30,000.</b>';}
if($_GET["30k"]=='40k') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tuition from $30,000 to $40,000.</b>';}
if($_GET["40k"]=='50k') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tuition from $40,000 to $50,000.</b>';}
if($_GET["50k"]=='60k') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tuition from $50,000 to $60,000.</b>';}
if($_GET["60k"]=='70k') {echo '<br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Tuition from $60,000 to $70,000.</b>';}

echo "<br><br></div>";

}

?>



</div>
<script lang="javascript">
qdbWrite();
</script>

</body>

</html>