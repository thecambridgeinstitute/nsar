//schema download: https://thecambridgeinstitute.quickbase.com/db/bhebcy4n5?act=API_Authenticate&username=internet@thecambridgeinstitute.org&password=helloeveryone111&a=API_GetSchema&ticket=6_bhwbuwdv3_bzqwqs_b_bgeyspdb7qucrmbeidxvabc6bfgscqpjqshduajnfjct29c3nc5ew6d6&apptoken=cwi4qrk9a5ad37s7rhcmtp86q

// Most of the main functionalities goes into the jquery closure
(function ($){
    window.hashtag = location.hash.replace( /^#/, '' ) || '95';
    window.hashtag = location.hash.replace( /^#/, '' ) || '22';
	processHashTag(window.hashtag);
    
    window.query = "";
    
    $(window).hashchange(function() {
        window.hashtag = location.hash.replace( /^#/, '' ) || '95';
		window.hashtag = location.hash.replace( /^#/, '' ) || '22';
        processHashTag(window.hashtag);
        changeNav(window.qid);
        
        clearSearch();
        destroyTable("#PSARTable");
        
        buildQueryInit();
        updateTable("bhebcy4n5", window.query, window.qid, "#PSARTable"); 
    });
    
    $(document).ready(function () {
        changeNav(window.qid);
        
        buildQueryInit();
        updateTable("bhebcy4n5", window.query, window.qid, "#PSARTable");
        buildTuitionSlider();
        
        bindEventHandlers();
    });
	
/*     function buildQueryInit() {
	 if (typeof(window.compareValue) !== "undefined") {
	   $("#psarNav22 > a").attr("href", "#22/" + window.compareValue);
            $("#clearSchools").show();
            buildPSARQuery();
        } else {
            $("#psarNav22 > a").attr("href", "#22");
            $("#clearSchools").hide();
            window.query = "";
        }
    }			
*/			
     function buildQueryInit() {
        if (typeof(window.compareValue) !== "undefined") {
            $("#psarNav95 > a").attr("href", "#95/" + window.compareValue);
            $("#psarNav22 > a").attr("href", "#22/" + window.compareValue);
            $("#clearSchools").show();
            buildPSARQuery();
        } else {
            $("#psarNav95 > a").attr("href", "#95");
            $("#psarNav22 > a").attr("href", "#22");
            $("#clearSchools").hide();
            window.query = "";
        }
    } 
    
    function bindEventHandlers() {
        $("#clearButton").click(function(){
            clearSearch();
        });
        
        $("#searchButton").click(function(){
            toggleSearch();
            buildPSARQuery(); 
            destroyTable("#PSARTable");
            updateTable("bhebcy4n5", window.query, window.qid, "#PSARTable");
        });
        
        $("#compareButton").click(function(){
            window.open("psmi.html#" + $("#compareInput").val());
        });
        
        $("#clearCompare").click(function(){
            $("#compareInput").val("");
            window.compareFilter = {};
            $(".compare-btn").each(function(idx, btn) {
                $(btn).html('<span class="glyphicon glyphicon-plus"></span>');
            });
        });
    }
    
    function buildTuitionSlider() {
        window.slider = $('#tuitionRangeSlider').slider({
            "min": 10000,
            "max": 70000,
            "step": 10000,
            "orientation": "horizontal",
            "value": [20000, 60000],
            "handle": "round"
        });
        window.slider.on("slide", function(e){
            $("#tuitionRangeValue").html($(".tooltip-inner").text());
            jQuery('#clearTuitionRange').show();
        });
    }
    
    function clearSearch() {
        $("#stateInput").val("");
        clearTuitionRange();
        $(".checkbox > input").removeAttr("checked");
        window.query = "";
        destroyTable("#PSARTable");
        updateTable("bhebcy4n5", window.query, window.qid, "#PSARTable");
    }
    
    function changeNav(qid) {
        $(".psarNav").removeClass("active");
        $("#psarNav" + qid).addClass("active");
        $("#clearSchools > a").attr("href", "#" + qid);
    }
    
    function processHashTag(hashtag) {
        var split_hashtag = hashtag.split("/");
        window.qid = split_hashtag[0];
        if (split_hashtag.length > 1) {
            window.compareValue = split_hashtag[1];
        } else {
            window.compareValue = undefined;
        }
    }
    
})(jQuery);

//functions that gets used by anchor tags
function destroyMap() {
    jQuery("#USMap").html("");
}

function buildMap() {
    jQuery("#USMap").vectorMap({
        map: 'us_aea_en',
        zoomOnScroll: false,
        onRegionClick: function(event, label) {
            //var state = jQuery(".jvectormap-label").text();
            //window.PSARTable.fnFilter(state);
            var state = label.split("-")[1];
            jQuery("#stateInput").val($.trim(jQuery("#stateInput").val() +  " " + state));
        }
    });
}

function toggleSearch() {
    var showSearchOptions = jQuery("#showSearchOptions");
    var searchOptions = jQuery("#searchOptions"); 
    if (showSearchOptions.hasClass("active")) {
        showSearchOptions.removeClass("active");
        searchOptions.hide();
        destroyMap();
    } else {
        showSearchOptions.addClass("active");
        searchOptions.show();
        buildMap();
    }
}

function clearTuitionRange() {
    jQuery('#tuitionRangeValue').html('');
    jQuery('#clearTuitionRange').hide();
    jQuery('#tuitionRangeSlider').slider('setValue', [20000, 60000]);
}